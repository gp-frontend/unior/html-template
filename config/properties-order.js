"use strict";

module.exports = [
  require("./groups/top_properties"),
  require("./groups/box_model"),
  require("./groups/positioning"),
  require("./groups/typography"),
  require("./groups/visual"),
  require("./groups/animation"),
  require("./groups/misc"),
];
